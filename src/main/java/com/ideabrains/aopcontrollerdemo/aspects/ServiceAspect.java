package com.ideabrains.aopcontrollerdemo.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class ServiceAspect {

    private static Logger log = LoggerFactory.getLogger(ServiceAspect.class);


    @Pointcut(" within(@org.springframework.stereotype.Service *)")
    public void service() {
    }


    @Around("service() ")
    public Object logBefore(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        long start = System.currentTimeMillis();
        try {
            String className = proceedingJoinPoint.getSignature().getDeclaringTypeName();
            String methodName = proceedingJoinPoint.getSignature().getName();
            Object result = proceedingJoinPoint.proceed();
            long elapsedTime = System.currentTimeMillis() - start;
            log.info("Method Service " + className + "." + methodName + " ()" + " execution time : "
                    + elapsedTime + " ms");

            return result;
        } catch (IllegalArgumentException e) {
            log.error("Illegal argument " + Arrays.toString(proceedingJoinPoint.getArgs()) + " in "
                    + proceedingJoinPoint.getSignature().getName() + "()");
            throw e;
        }

    }
}

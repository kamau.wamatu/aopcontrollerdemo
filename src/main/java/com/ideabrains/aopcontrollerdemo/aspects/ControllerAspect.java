package com.ideabrains.aopcontrollerdemo.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class ControllerAspect {

    private static Logger logger = LoggerFactory.getLogger(ControllerAspect.class);


    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void controller() {
    }


    @Around("controller() ")
    public Object logBefore(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        long start = System.currentTimeMillis();
        try {
            String className = proceedingJoinPoint.getSignature().getDeclaringTypeName();
            String methodName = proceedingJoinPoint.getSignature().getName();
            Object result = proceedingJoinPoint.proceed();
            long elapsedTime = System.currentTimeMillis() - start;
            logger.info("Method Derrick " + className + "." + methodName + " ()" + " execution time : "
                    + elapsedTime + " ms");

            return result;
        } catch (IllegalArgumentException e) {
            logger.error("Illegal argument " + Arrays.toString(proceedingJoinPoint.getArgs()) + " in "
                    + proceedingJoinPoint.getSignature().getName() + "()");
            throw e;
        }

    }
}

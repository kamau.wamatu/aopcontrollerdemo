package com.ideabrains.aopcontrollerdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AopcontrollerdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AopcontrollerdemoApplication.class, args);
    }

}

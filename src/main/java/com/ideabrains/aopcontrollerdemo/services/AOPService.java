package com.ideabrains.aopcontrollerdemo.services;

import org.springframework.stereotype.Service;

@Service
public class AOPService {


    public String processRequest() {

        return "processed";
    }
}


package com.ideabrains.aopcontrollerdemo.controllers;

import com.ideabrains.aopcontrollerdemo.services.AOPService;
import com.ideabrains.aopcontrollerdemo.aspects.annotations.Loggable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/process")
public class AOPController {

    private final AOPService aopService;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    public AOPController(AOPService aopService
    ) {
        this.aopService = aopService;
    }


    @RequestMapping("/log")
    @Loggable
    public String processRequest() {

        log.info("Processed  " );
        return aopService.processRequest();
    }
}
